Apache Cassandra Notes
----------------------
https://www.youtube.com/watch?v=B_HTdrTgGNs
https://www.youtube.com/playlist?list=PLalrWAGybpB-L1PGA-NfFu2uiWHEsdscD
----------------------

non-relational

nodes:
	are in a cluster
	Share nothing
	Add or remove as needed
	more capacity can be achieved by adding a server

unbelievably scalable, almost perefectly linear

Client writes local
Data sync across WAN
Replication Factor per DC (asynch)

writes to a single node
server acknowledges datawrite to client (AOF)

Not "in memory"
	client -> memtable -> acknowledge -> flush (writes memtable to sstable(sorted string table))

Cassandra is "sequential IO" not "random IO"

Writes are immutable - once a write is done, it will never write to that file again

When you "overwrite" data, a "merge sort" occurs and a new file is made.
	sstables do a sequential read of both files and merge sort, favoring the most recent entry

Compaction occurs constantly during reads and writes

Fully distributed - no single point of failure

Cluster Replication is important

Virtual Nodes 
	physical servers are virtualized
	smaller non-adjacent ranges
	paralell writes and reads

Reads are coordinated
	mutation -> read -> node looks for data via hash -> potentially asks other nodes -> ackn to client

Loss of a node shouldn't be an issue

QUORUM -> 51% replicas ack
	rapid read protection

CQL cassandra query language:
	no sizes ie: varchar != varchar(255)
	first item in PK is the "partition key" ??

Insert always overwrites

To expire -
	USING TTL 10238958349
		where the number is in seconds
















